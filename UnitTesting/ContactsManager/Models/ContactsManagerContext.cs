using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ContactsManager.Models
{
    public class ContactsManagerContext : DbContext, IRepository<Contact, int>
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<ContactsManager.Models.ContactsManagerContext>());

        public ContactsManagerContext() : base("name=ContactsManagerContext")
        {
        }

        public DbSet<ContactsManager.Models.Contact> Contacts { get; set; }

        IQueryable<Contact> IRepository<Contact, int>.FindAll()
        {
            return this.Contacts;
        }

        Contact IRepository<Contact, int>.Find(int id)
        {
            return this.Contacts.Find(id);
        }

        void IRepository<Contact, int>.Create(Contact entity)
        {
            this.Contacts.Add(entity);
            this.SaveChanges();
        }

        void IRepository<Contact, int>.Update(Contact entity)
        {
            this.Entry(entity).State = EntityState.Modified;
            this.SaveChanges();
        }

        void IRepository<Contact, int>.Delete(int id)
        {
            Contact entity = this.Contacts.Find(id);
            this.Contacts.Remove(entity);
            this.SaveChanges();
        }
    }
}