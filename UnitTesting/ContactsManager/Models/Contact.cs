﻿using System;

namespace ContactsManager.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public DateTime Birthday { get; set; }
    }
}