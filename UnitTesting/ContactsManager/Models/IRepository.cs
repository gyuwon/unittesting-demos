﻿using System.Linq;

namespace ContactsManager.Models
{
    public interface IRepository<TEntity, TId>
    {
        IQueryable<TEntity> FindAll();
        TEntity Find(TId id);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TId id);
    }
}
