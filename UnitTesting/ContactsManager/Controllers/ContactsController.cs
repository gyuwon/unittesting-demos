using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContactsManager.Models;

namespace ContactsManager.Controllers
{
    public class ContactsController : Controller
    {
        private readonly IRepository<Contact, int> repo;

        public ContactsController(IRepository<Contact, int> repo)
        {
            this.repo = repo;
        }

        //
        // GET: /Contacts/
        public ActionResult Index()
        {
            return View(repo.FindAll().ToList());
        }

        //
        // GET: /Contacts/Details/5
        public ActionResult Details(Int32 id)
        {
            Contact contact = repo.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // GET: /Contacts/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Contacts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Contact contact)
        {
            if (ModelState.IsValid)
            {
                repo.Create(contact);
                return RedirectToAction("Index");
            }

            return View(contact);
        }

        //
        // GET: /Contacts/Edit/5
        public ActionResult Edit(Int32 id)
        {
            Contact contact = repo.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // POST: /Contacts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Contact contact)
        {
            if (ModelState.IsValid)
            {
                this.repo.Update(contact);
                return RedirectToAction("Index");
            }
            return View(contact);
        }

        //
        // GET: /Contacts/Delete/5
        public ActionResult Delete(Int32 id)
        {
            Contact contact = repo.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //
        // POST: /Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Int32 id)
        {
            this.repo.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            IDisposable disposable = this.repo as IDisposable;
            if (disposable != null)
                disposable.Dispose();
            base.Dispose(disposing);
        }
    }
}
