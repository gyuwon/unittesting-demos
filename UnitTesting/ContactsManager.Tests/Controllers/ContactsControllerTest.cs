﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactsManager.Controllers;
using ContactsManager.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ContactsManager.Tests.Controllers
{
    [TestClass]
    public class ContactsControllerTest
    {
        [TestMethod]
        public void Index()
        {
            var mock = new Mock<IRepository<Contact, int>>();

            ContactsController controller = new ContactsController(mock.Object);
            var result = controller.Index();

            mock.Verify(r => r.FindAll());
        }
    }
}
