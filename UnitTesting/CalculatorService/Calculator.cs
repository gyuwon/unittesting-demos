﻿using System;

namespace CalculatorService
{
    public class Calculator
    {
        public double Add(double x, double y)
        {
            return x + y;
        }

        public double Multiply(double x, double y)
        {
            return x * y;
        }
    }
}
