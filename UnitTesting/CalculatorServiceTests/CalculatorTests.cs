﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorService;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorService.Tests
{
    [TestClass]
    public class CalculatorTests
    {
        [TestMethod]
        public void AddTest()
        {
            Calculator calculator = new Calculator();
            double x = 10, y = 20;
            double expected = 30;
            double actual = calculator.Add(x, y);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void MultiplyTest()
        {
            Calculator calculator = new Calculator();
            double x = 5, y = 0.5;
            double expected = 2.5;
            double actual = calculator.Multiply(x, y);
            Assert.AreEqual(expected, actual);
        }
    }
}
